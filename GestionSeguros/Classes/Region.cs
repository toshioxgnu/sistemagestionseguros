﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestionSeguros.Classes
{
    public class Region
    {
        private string Nombre { get; set; }
        private string[] Comunas { get; set; }

        public Region()
        {

        }

        public Region(string nombre, string[] comunas)
        {
            Nombre = nombre;
            Comunas = comunas;
        }
    }
}