﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebGrease.Css.Ast;

namespace GestionSeguros.Classes
{
    public class Utilidades
    {
        public bool validaRut(string rut)
        {
            int[] constantesValidacion = { 3, 2, 7, 6, 5, 4, 3, 2 };
            double suma = 0;

            if (rut.Length == 9)
            {
                rut = "0" + rut;
            }

            for (int j = 0; j < constantesValidacion.Length; j++)
            {
                suma = suma + (Int16.Parse(rut[j].ToString()) * constantesValidacion[j]);

            }

            double division = suma / 11;
            int divisionentero = (int)division;
            double resto = division - divisionentero;
            double resta = 11 - (11 * resto);
            Console.WriteLine(resta);
            var digito = Math.Round(resta);

            string digitorut = rut[9].ToString();

            if (digitorut == "k")
            {
                digitorut = "9";
            }
            else if (digitorut == "0")

            {
                digitorut = "11";
            }


            if (digito.ToString() == digitorut)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}
