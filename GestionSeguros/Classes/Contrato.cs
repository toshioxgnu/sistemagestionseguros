﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestionSeguros.Classes
{
    public class Contrato
    {
        private int id;
        private string rut;
        private string nombre;
        private DateTime Finicio;
        private DateTime Ftermino;
        private string patente;
        private string marca;
        private string modelo;
        private int anio;
        private int idVendedor;

        public Contrato(int id, string rut, string nombre, DateTime finicio, DateTime ftermino, string patente, string marca, string modelo, int anio, int idVendedor)
        {
            this.id = id;
            this.rut = rut;
            this.nombre = nombre;
            Finicio = finicio;
            Ftermino = ftermino;
            this.patente = patente;
            this.marca = marca;
            this.modelo = modelo;
            this.anio = anio;
            this.idVendedor = idVendedor;
        }

        public Contrato()
        {

        }

        public int Id
        {
            get => id;
            set => id = value;
        }

        public string Rut
        {
            get => rut;
            set => rut = value;
        }

        public string Nombre
        {
            get => nombre;
            set => nombre = value;
        }

        public DateTime Finicio1
        {
            get => Finicio;
            set => Finicio = value;
        }

        public DateTime Ftermino1
        {
            get => Ftermino;
            set => Ftermino = value;
        }

        public string Patente
        {
            get => patente;
            set => patente = value;
        }

        public string Marca
        {
            get => marca;
            set => marca = value;
        }

        public string Modelo
        {
            get => modelo;
            set => modelo = value;
        }

        public int Anio
        {
            get => anio;
            set => anio = value;
        }

        public int IdVendedor
        {
            get => idVendedor;
            set => idVendedor = value;
        }
    }
}