﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestionSeguros.Classes
{
    public class Contacto
    {
        private string Nombres { get; set; }
        private string Apellidos { get; set; }
        private string Email { get; set; }
        private string Direccion { get; set; }
        private string Direccion2 { get; set; }
        private string Pais { get; set; }
        private string Region { get; set; }
        private string Comuna { get; set; }
        private string Interes { get; set; }

        public Contacto(string nombres, string apellidos, string email, string direccion, string direccion2,
            string pais, string region, string comuna, string interes)
        {
            Nombres = nombres;
            Apellidos = apellidos;
            Email = email;
            Direccion = direccion;
            Direccion2 = direccion2;
            Pais = pais;
            Region = region;
            Comuna = comuna;
            Interes = interes;
        }

        public Contacto()
        {

        }
    }
}