﻿namespace GestionSeguros.Classes
{
    public class User
    {
        private int id;
        private string rut;
        private string nombre;
        private string appat;
        private string apmat;
        private string pass;
        private string email;
        private string permisos;

        public User()
        {

        }
        

        public User(int id, string rut, string nombre, string appat, string apmat, string pass, string email, string permisos)
        {
            this.id = id;
            this.rut = rut;
            this.nombre = nombre;
            this.appat = appat;
            this.apmat = apmat;
            this.pass = pass;
            this.email = email;
            this.permisos = permisos;
        }

        public int Id
        {
            get => id;
            set => id = value;
        }

        public string Rut
        {
            get => rut;
            set => rut = value;
        }

        public string Nombre
        {
            get => nombre;
            set => nombre = value;
        }

        public string Appat
        {
            get => appat;
            set => appat = value;
        }

        public string Apmat
        {
            get => apmat;
            set => apmat = value;
        }

        public string Pass
        {
            get => pass;
            set => pass = value;
        }

        public string Email
        {
            get => email;
            set => email = value;
        }

        public string Permisos
        {
            get => permisos;
            set => permisos = value;
        }
    }
}