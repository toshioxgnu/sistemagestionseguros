﻿using MySql.Data.MySqlClient;

namespace GestionSeguros.Classes
{
    public class BD
    {
        private MySqlConnection conn;
        private string server;
        private string database;
        private string uid;
        private string password;


        public MySqlConnection Conectar()
        {
            server = "localhost";
            database = "gestion_seguros";
            uid = "developer";
            password = "Lkl15963";
            string connString = "SERVER=" + server + ";DATABASE=" + database + ";UID=" + uid + ";PASSWORD="+password+";";
            conn = new MySqlConnection(connString);
            return conn;
        }

        public MySqlDataReader consultaSelect(string sql)
        {
            User usuario = new User();
            MySqlConnection con = Conectar();
            con.Open();
            MySqlCommand cmd = new MySqlCommand(sql, con);
            MySqlDataReader rdr = cmd.ExecuteReader();
            return rdr;
           
        }

        public bool ejecutaInsert(string sql)
        {
            MySqlConnection con = Conectar();
            con.Open();
            MySqlCommand cmd = new MySqlCommand(sql, con);
            cmd.ExecuteNonQuery();
            return true;
        }
        public bool ejecutaDelete(string sql)
        {
            MySqlConnection con = Conectar();
            con.Open();
            MySqlCommand cmd = new MySqlCommand(sql, con);
            cmd.ExecuteNonQuery();
            return true;
        }
        public bool ejecutaUpdate(string sql)
        {
            MySqlConnection con = Conectar();
            con.Open();
            MySqlCommand cmd = new MySqlCommand(sql, con);
            cmd.ExecuteNonQuery();
            return true;
        }
    }
}