﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using GestionSeguros.Classes;
using Microsoft.Ajax.Utilities;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;

namespace GestionSeguros.Controllers
{
    public class AdminController : Controller
    {
        Utilidades utils = new Utilidades();
        [HttpPost]
        public string getUsuarios(string accion)
        {
            string tabla = "";
            if (accion == "getUsuarios")
            {
                string sql = "select * from usuarios";
                BD bd = new BD();
                MySqlDataReader rdr = bd.consultaSelect(sql);
                string permiso = "";
                string stringpermiso = "";
                while (rdr.Read())
                {
                    tabla = tabla + "<tr>";
                    tabla = tabla + "<td>"+rdr.GetValue(0)+"</td>";
                    tabla = tabla + "<td>"+rdr.GetValue(1)+"</td>";
                    tabla = tabla + "<td>"+rdr.GetValue(2)+" "+ rdr.GetValue(3) +" "+ rdr.GetValue(4) +"</td>";
                    tabla = tabla + "<td>"+rdr.GetValue(6)+ "</td>";
                    stringpermiso = rdr.GetValue(7).ToString();
                    if( stringpermiso[0] == '1')
                    {
                        permiso = "Admin";
                    }else if (stringpermiso[1] == '1')
                    {
                        permiso = "Vendedor";
                    }else if (stringpermiso[2] == '1')
                    {
                        permiso = "cliente";
                    }

                    tabla = tabla + "<td>"+permiso+ "</td>";
                    tabla = tabla + "<td><button class='btn btn-danger' onclick='eliminaUsuario("+rdr.GetValue(0)+ ")'><i class='fas fa-trash'></i></button><button class='btn btn-warning' onclick='getUsuario(" + rdr.GetValue(0) + ")'><i class='far fa-edit'></i></button></td>";

                }

            }
            return tabla;
        }

        [HttpPost]
        public bool agregaUsuario(string rut, string nombre, string appat, string apmat, string pass, string email, string permisos)
        {
            if (utils.validaRut(rut))
            {
                string sql = "insert into usuarios ( RUT, NOMBRE, APPAT, APMAT, PASS, EMAIL, PERMISOS) values ('" + rut + "','" + nombre + "','" + appat + "','" + apmat + "','" + pass + "','" + email + "','" + permisos + "')";
                BD bd = new BD();
                return bd.ejecutaInsert(sql);
            }
            else
            {
                return false;
            }
            
        }

        [HttpPost]
        public bool eliminaUsuario(string id)
        {
            string sql = "delete from usuarios where ID = "+id;
            BD bd = new BD();
            return bd.ejecutaDelete(sql);
        }

        [HttpPost]
        public string getUsuario(string id)
        {
            User usuarioedita = new User();
            string sql = "select * from usuarios where ID = "+id;
            BD bd = new BD();
            MySqlDataReader rdr = bd.consultaSelect(sql);
            while (rdr.Read())
            {
                usuarioedita.Id = (int)rdr.GetValue(0);
                usuarioedita.Rut = (string)rdr.GetValue(1);
                usuarioedita.Nombre = (string)rdr.GetValue(2);
                usuarioedita.Appat = (string)rdr.GetValue(3);
                usuarioedita.Apmat = (string)rdr.GetValue(4);
                usuarioedita.Pass = (string)rdr.GetValue(5);
                usuarioedita.Email = (string)rdr.GetValue(6);
                usuarioedita.Permisos = (string)rdr.GetValue(7);
            }

            var json = new JavaScriptSerializer().Serialize(usuarioedita);
            return json;
        }

        [HttpPost]
        public bool editaUsuario(string id, string rut,  string nombre, string appat, string apmat, string pass, string email, string permisos)
        {
            string sql = "update usuarios set RUT = '"+rut+"', NOMBRE = '"+nombre+"', APPAT = '"+appat+"', APMAT = '"+apmat+"', PASS = '"+pass+"', EMAIL = '"+email+"',PERMISOS = '"+permisos+"' where ID = "+id+"";
            BD bd = new BD();
            return bd.ejecutaUpdate(sql);
        }
    }
}