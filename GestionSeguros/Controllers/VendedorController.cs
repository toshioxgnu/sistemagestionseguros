﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using GestionSeguros.Classes;
using MySql.Data.MySqlClient;

namespace GestionSeguros.Controllers
{
    public class VendedorController : Controller
    {
        private Utilidades Utils = new Utilidades();
        [HttpPost]
        public bool generaVenta(string rut, string nombre, string appat, string apmat, string email, string marca, string modelo, string anio, string pat, string idvendedor)
        {
            bool exito = false;
            if (Utils.validaRut(rut))
            {
                string passinicial = "";
                for (int i = 0; i < 4; i++)
                {
                    passinicial = passinicial + rut[i];
                }
                string sql = "insert into usuarios ( RUT, NOMBRE, APPAT, APMAT, PASS, EMAIL, PERMISOS) values ('" + rut + "','" + nombre + "','" + appat + "','" + apmat + "','" + passinicial + "','" + email + "','001')";
                string sql2 = "insert into seguros ( RUT_ASEGURADO, NOMBRE_ASEGURADO, FINICIO, FTERMINO, PATENTE, MARCA, MODELO, ANIO, FECHA_INSERT, FECHA_EMISION, ID_VENDEDOR) values( '" + rut + "','" + nombre + " " + appat + " " + apmat + "',NOW(), DATE_ADD(NOW(), INTERVAL 1 YEAR),'" + pat + "','" + marca + "','" + modelo + "'," + anio + ", NOW(), NOW(), " + idvendedor + " )";
                BD bd = new BD();
                if (bd.ejecutaInsert(sql) && bd.ejecutaInsert(sql2))
                {
                    exito =  true;
                }
            }
            else
            {
                exito =  false;
            }

            return exito;
        }

        [HttpPost]
        public string getVentas(string idvendedor)
        {
            string tabla = "";
            string filtro = "";
            string vendedor = "where ID_VENDEDOR = " + idvendedor;
            if (idvendedor != "")
            {
                filtro = vendedor;
            }
            else
            {
                filtro = "";
            }
            string sql = "select * from seguros "+filtro;
            BD bd = new BD();
            MySqlDataReader rdr = bd.consultaSelect(sql);
            while (rdr.Read())
            {
                tabla = tabla + "<tr>";
                tabla = tabla + "<td>" + rdr.GetValue(0) + "</td>";
                tabla = tabla + "<td>" + rdr.GetValue(1) + "</td>";
                tabla = tabla + "<td>" + rdr.GetValue(2) +"</td>";
                tabla = tabla + "<td>" + rdr.GetValue(3) + "</td>";
                tabla = tabla + "<td>" + rdr.GetValue(4) + "</td>";
                tabla = tabla + "<td>" + rdr.GetValue(6) +" "+ rdr.GetValue(7) +" "+rdr.GetValue(8)+ "</td>";
                tabla = tabla + "<td>" + rdr.GetValue(5) + "</td>";
                tabla = tabla + "<td><button class='btn btn-danger' onclick='eliminacontrato(" + rdr.GetValue(0) + ")'><i class='fas fa-trash'></i></button><button class='btn btn-warning' onclick='getContrato(" + rdr.GetValue(0) + ")'><i class='far fa-edit'></i></button></td>";

            }
            return tabla;
        }

        [HttpPost]
        public bool eliminacontrato(string id)
        {
            string sql = "delete from seguros where ID = " + id;
            BD bd = new BD();
            return bd.ejecutaDelete(sql);
        }

        [HttpPost]
        public string getContrato(string id)
        {
            Contrato contratoedita = new Contrato();
            string sql = "select * from seguros where ID = " + id;
            BD bd = new BD();
            MySqlDataReader rdr = bd.consultaSelect(sql);
            while (rdr.Read())
            {
                contratoedita.Id = (int)rdr.GetValue(0);
                contratoedita.Rut = (string)rdr.GetValue(1);
                contratoedita.Nombre = (string)rdr.GetValue(2);
                contratoedita.Finicio1 = (DateTime) rdr.GetValue(3);
                contratoedita.Ftermino1 = (DateTime) rdr.GetValue(4);
                contratoedita.Patente = (string) rdr.GetValue(5);
                contratoedita.Marca = (string) rdr.GetValue(6);
                contratoedita.Modelo = (string) rdr.GetValue(7);
                contratoedita.Anio = (int) rdr.GetValue(8);
                contratoedita.IdVendedor = (int) rdr.GetValue(11);
            }

            var json = new JavaScriptSerializer().Serialize(contratoedita);
            return json;
        }

    }
}