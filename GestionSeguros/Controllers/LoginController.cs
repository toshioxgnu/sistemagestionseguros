﻿using System.Web.Mvc;
using GestionSeguros.Classes;
using MySql.Data.MySqlClient;

namespace GestionSeguros.Controllers
{
    public class LoginController : Controller
    {
         User userlogeado = new User();

        public ActionResult login(string email, string pass)
        {
            BD bd = new BD();
            var sql = "select * from usuarios where EMAIL = '" + email + "' and PASS = '" + pass + "'";
            MySqlDataReader data = bd.consultaSelect(sql);
           
            while (data.Read())
            {
                userlogeado.Id = (int) data.GetValue(0);
                userlogeado.Rut = (string) data.GetValue(1);
                userlogeado.Nombre = (string) data.GetValue(2);
                userlogeado.Appat = (string) data.GetValue(3);
                userlogeado.Apmat = (string) data.GetValue(4);
                userlogeado.Pass = (string) data.GetValue(5);
                userlogeado.Email = (string) data.GetValue(6);
                userlogeado.Permisos = (string) data.GetValue(7);
            }

            ViewBag.session = userlogeado.Nombre;
            ViewBag.id = userlogeado.Id;
            ViewBag.Rut = userlogeado.Rut;

            if(userlogeado.Permisos[0] == '1')
            {
                return View("/Views/Admin/Admin.cshtml");
            }
            if (userlogeado.Permisos[1] == '1')
            {
                return View("/Views/Vendedor/vendedor.cshtml");
            }
            if (userlogeado.Permisos[2] == '1')
            {
                return View("/Views/Cliente/cliente.cshtml");
            }

            return null;

        }
    }
}