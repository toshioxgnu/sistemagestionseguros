﻿using System;
using System.Collections.Generic;
using System.EnterpriseServices;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using GestionSeguros.Classes;
using MySql.Data.MySqlClient;

namespace GestionSeguros.Controllers
{
    public class ClienteController : Controller
    {
        User usercliente = new User();

        public string getCliente(string idcliente)
        {
            string tabla = "";
            BD bd = new BD();
            var sql = "select * from usuarios where ID = "+idcliente;
            MySqlDataReader data = bd.consultaSelect(sql);

            while (data.Read())
            {
                usercliente.Id = (int)data.GetValue(0);
                usercliente.Rut = (string)data.GetValue(1);
                usercliente.Nombre = (string)data.GetValue(2);
                usercliente.Appat = (string)data.GetValue(3);
                usercliente.Apmat = (string)data.GetValue(4);
                usercliente.Pass = (string)data.GetValue(5);
                usercliente.Email = (string)data.GetValue(6);
                usercliente.Permisos = (string)data.GetValue(7);
            }

            tabla = "<tr>"; 
            tabla = tabla + "<td>"+usercliente.Rut+"</td>"; 
            tabla = tabla + "<td>"+usercliente.Nombre+" "+usercliente.Appat+" "+usercliente.Apmat+"</td>"; 
            tabla = tabla + "<td>"+usercliente.Email+"</td>";
            tabla = tabla + "</tr>";
            tabla = tabla + "-/-/-/-"+usercliente.Nombre;

            return tabla;

        }

        [HttpPost]
        public string getVehiculo(string rut)
        {
            string auto = "";
            string tabla = "";
            string filtro = "";

            string sql = "select * from seguros where RUT_ASEGURADO = '" + rut+"'";
            BD bd = new BD();
            MySqlDataReader rdr = bd.consultaSelect(sql);
            while (rdr.Read())
            {
                tabla = tabla + "<tr>";
                tabla = tabla + "<td>" + rdr.GetValue(5) + "</td>";
                tabla = tabla + "<td>" + rdr.GetValue(6) + "</td>";
                tabla = tabla + "<td>" + rdr.GetValue(7) + "</td>";
                tabla = tabla + "<td>" + rdr.GetValue(8) + "</td>";
                tabla = tabla + "<td>" + rdr.GetValue(4) + "</td>";
                tabla = tabla + "</tr>";
                tabla = tabla + "-/-/-/-"+ rdr.GetValue(6) + "-/-/-/-" + rdr.GetValue(7);

            }
            return tabla;
        }
    }
}