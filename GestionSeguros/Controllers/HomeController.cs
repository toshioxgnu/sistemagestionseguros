﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GestionSeguros.Classes;
using Newtonsoft.Json;

namespace GestionSeguros.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Login()
        {
            return View("/Views/Login/login.cshtml");
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }

        [HttpPost]
        public bool agregaContacto()
        {

            return true;
        }

        [HttpPost]
        public string getRegiones()
        {
            string opciones = "";

            dynamic result = JsonConvert.DeserializeObject(@"c:json\movie.json");

            var regiones = new List<Region>();

            foreach (var file in result.version.files)
            {
                regiones.Add(file.url);
            }

            foreach (var region in regiones)
            {
                opciones = opciones + "<option>" + region + "</option>";
            }

            return opciones;
        }
    }
}