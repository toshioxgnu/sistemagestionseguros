﻿
$(document).ready(function () {

});

function getUsuarios() {
	$.ajax({
        type: 'POST',
        url: "/Admin/getUsuarios",
        data: {
            accion: "getUsuarios"
        }
        , success: function (data) {
            $("#usuarios").html(data);
        }
    });

}

function agregaUsuario() {
    $.ajax({
        type: 'POST',
        url: "/Admin/agregaUsuario",
        data: {
            rut: $("#rut").val(),
            nombre: $("#nombre").val(),
            appat: $("#appat").val(),
            apmat: $("#apmat").val(),
            pass: $("#pass").val(),
            email: $("#email").val(),
            permisos: $("#permisos").val()
        }
        , success: function (data) {
            if (data) {
                alert("El usuario fue agregado correctamente");
                getUsuarios();
            } else {
                getUsuarios();
                alert("El rut ingresado es incorrecto");
            }
            
        }
    });

}

function eliminaUsuario(id) {
    $.ajax({
        type: 'POST',
        url: "/Admin/eliminaUsuario",
        data: {
            id: id
        }
        , success: function () {
            getUsuarios();
        }
    });

}

function getUsuario(id) {
    $("#agregaedita").trigger("click");
    $("#agrega").hide();
    $.ajax({
        type: 'POST',
        url: "/Admin/getUsuario",
        dataType: "JSON",
        data: {
            id: id
        }
        , success: function (data) {
            console.log(data);
            $("#rut").val(data.Rut);
            $("#idusuario").val(data.Id);
            $("#nombre").val(data.Nombre);
            $("#appat").val(data.Appat);
            $("#apmat").val(data.Apmat);
            $("#email").val(data.Email);
            $("#pass").val(data.Pass);
            $("#permisos").val(data.Permisos);
        }
    });
}

function editaUsuario() {
    $.ajax({
        type: 'POST',
        url: "/Admin/editaUsuario",
        data: {
            id: $("#idusuario").val(),
            rut: $("#rut").val(),
            nombre: $("#nombre").val(),
            appat: $("#appat").val(),
            apmat: $("#apmat").val(),
            pass: $("#pass").val(),
            email: $("#email").val(),
            permisos: $("#permisos").val()
        }
        , success: function () {
            getUsuarios();
        }
    });

}

