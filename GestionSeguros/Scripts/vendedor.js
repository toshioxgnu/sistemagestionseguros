﻿$(document).ready(function() {

});

function agregaVenta() {
    $.ajax({
        type: 'POST',
        url: "/Vendedor/generaVenta",
        data: {
            rut: $("#rut").val(),
            nombre: $("#nombre").val(),
            appat: $("#appat").val(),
            apmat: $("#apmat").val(),
            email: $("#email").val(),
            pat: $("#patente").val(),
            marca: $("#marca").val(),
            modelo: $("#modelo").val(),
            anio: $("#anio").val(),
            idvendedor: $("#idvendedor").val()
        }
        , success: function (data) {
           if (data) {
               alert("Venta registrada");
           } else {
               alert("Error en registrar la venta");
            }
           getVentas();
        }
    });

}

function getVentas() {
    $.ajax({
        type: 'POST',
        url: "/Vendedor/getVentas",
        data: {
            idvendedor: $("#idvendedor").val()
        }
        , success: function (data) {
            $("#seguros").html(data);
        }
    });
}

function eliminacontrato(id) {
    $.ajax({
        type: 'POST',
        url: "/Vendedor/eliminaContrato",
        data: {
            id: id
        }
        , success: function () {
            getVentas();
        }
    });

}

function getContrato(id) {
    $("#registraventa").trigger("click");
    $("#agrega").hide();
    $.ajax({
        type: 'POST',
        url: "/Vendedor/getContrato",
        dataType: "JSON",
        data: {
            id: id
        }
        , success: function (data) {
            console.log(data);
            let nombre = data.Nombre.split(" ");
            console.log(nombre[1]);
            $("#rut2").val(data.Rut);
            $("#nombre2").val(nombre[0]);
            $("#appat2").val(nombre[1]);
            $("#apmat2").val(nombre[2]);
            $("#patente2").val(data.Patente);
            $("#marca2").val(data.Marca);
            $("#modelo2").val(data.Modelo);
            $("#anio2").val(data.Anio);
        }
    });
}