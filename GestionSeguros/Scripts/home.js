﻿let json = "../../Content/json/regionescomunas.json";

$(document).ready(function () {
	getRegiones();

});

function getRegiones() {
	// arma el ajax de la variable ruta con una funcion incorporada
	$.getJSON(json, function(json) {
		// vacia el select
		$('#region').html('');
		// cada array del parametro tiene un elemento index(concepto) y un elemento value(el  valor de concepto)
		$.each(json.region, function(index, value) {
			// darle un option con los valores asignados a la variable select
			$(this).append('<option id="' + value.region + '">' + value.region + '</option>');
		});
		$(this).selectpicker('refresh');
	});
}
