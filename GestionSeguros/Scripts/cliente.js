﻿$(document).ready(function() {
    getCliente();
    getVehiculo();
});

function getCliente() {
    $.ajax({
        type: 'POST',
        url: "/Cliente/getCliente",
        data: {
            idcliente: $("#idcliente").val()
        }
        , success: function (data) {
            console.log(data);
            datos = data.split("-/-/-/-");
            $("#datoscliente").html(datos[0]);
            $("#nombrecliente").html(datos[1]);
        }
    });
}

function getVehiculo() {
    $.ajax({
        type: 'POST',
        url: "/Cliente/getVehiculo",
        data: {
            rut: $("#rutcliente").val()
        }
        , success: function (data) {
            console.log(data);
            datos = data.split("-/-/-/-");
            $("#datosvehiculo").html(datos[0]);
            $("#nombreauto").html(datos[1]+" "+datos[2]);
        }
    });
}