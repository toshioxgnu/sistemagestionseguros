create table USUARIOS (
    ID INT PRIMARY KEY AUTO_INCREMENT,
    RUT varchar(20),
    NOMBRE varchar(50),
    APPAT varchar(50),
    APMAT varchar(50),
    PASS  varchar(50),
    EMAIL varchar(50),
    PERMISOS varchar(3)
)

create table CONTACTO (
    NOMBRES varchar(100),
    APELLIDOS VARCHAR(100),
    EMAIL varchar(50),
    DIRECCION varchar(100),
    DIRECCCION2 varchar(100) default NULL,
    PAIS varchar(50),
    REGION varchar(100),
    COMUNA varchar(100)
)

create table SEGUROS(
    ID int primary key auto_increment,
    RUT_ASEGURADO varchar(20),
    NOMBRE_ASEGURADO varchar(100),
    FINICIO DATE,
    FTERMINO DATE,
    PATENTE VARCHAR(10),
    MARCA varchar(50),
    MODELO varchar(100),
    ANIO INT,
    FECHA_INSERT DATETIME,
    FECHA_EMISION datetime
)
